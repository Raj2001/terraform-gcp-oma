resource "google_compute_instance" "gce_instance" {
  #name         = var.track3_sql_server1
  name         = "default"
  machine_type = "n1-standard-1"
  zone         = "us-central1-a"
  #labels       = var.track3_sql_server1_labels
  tags = ["sql", "sql-ad", "track3-jump-host", "sql-track3"]
  
  boot_disk {
    initialize_params {
      image = "rhel-cloud/rhel-7"
          size = 20
    }
  }
 

  network_interface {
    subnetwork = "host-project-subnet-a"

    
  }

  metadata = {
    team = "track3"
  }

}
